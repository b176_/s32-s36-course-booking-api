const express = require('express')
const router = express.Router()
const controller = require('../controllers/courses')
const auth = require('../auth')
const { route } = require('./users')
const { request } = require('express')

// destructure the actual functions needed
const { verify, verifyAdmin } = auth

router.post('/create', verify, verifyAdmin, (request, response) => {
    controller.addOne(request.body).then(result => {
        response.send(result)
    })
})

router.get('/', verify, verifyAdmin, (request, response) => {
    controller.getAll().then(result => response.send(result))
})

router.get('/active', (request, response) => {
    controller.getAllActive().then(result => response.send(result))
})

router.get('/:id', (request, response) => {
    controller.getOne(request.params.id).then(result => response.send(result))
})

router.patch('/:id/update', verify, verifyAdmin, (request, response) => {
    controller.updateOne(request.params.id, request.body).then(result => response.send(result))
})

router.patch('/:id/archive', verify, verifyAdmin, (request, response) => {
    controller.archiveOne(request.params.id).then(result => {
        response.send(result)
    })
})

module.exports = router