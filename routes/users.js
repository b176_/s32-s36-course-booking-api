const express = require('express')
const controller = require('../controllers/users')
const auth = require('../auth')

const { verify, verifyAdmin } = auth

const router = express.Router()

router.post('/register', (request, response) => {
    let new_user = request.body

    controller.register(new_user).then(result => {
        response.send(result)
    })
})

router.post('/login', (request, response) => {
    controller.login(request.body).then(result => {
        response.send(result)
    })
})

// set verification process before proceeding to controller
router.get("/details", auth.verify, (req, res) => {
    controller.getUser(req.user.id)
        .then(result => res.send(result))
        .catch(err => res.send(err.message));
});

router.post('/enroll', verify, controller.enroll)


module.exports = router