const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const user_routes = require('./routes/users')
const course_routes = require('./routes/courses')

dotenv.config(); //setup the ENV

let account = process.env.CREDENTIALS
const port = process.env.PORT 

// Server Setup
const app = express()
app.use(express.json())
app.use(cors())

// let cors_options = {
//     origin: ['http://localhost:3000']
// }

// Routing
app.use('/users', user_routes)
app.use('/courses', course_routes)

// Database Connection 
mongoose.connect(account);
const connectStatus = mongoose.connection; 
connectStatus.once('open', () => console.log(`Database Connected`));

// Run server
app.get('/', (request, response) => {
    response.send('Welcome to Iskwela!')
})

app.listen(port, () => {
    console.log(`API is running on port ${port}`)
})