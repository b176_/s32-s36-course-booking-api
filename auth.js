const jwt = require('jsonwebtoken')

const secret_key = 'IskwelaAPI'

module.exports.createAccessToken = (user) => {
    console.log(user)

    // payload
    const data = {
        id: user._id,
        email: user.email,
        is_admin: user.is_admin
    }

    // generate signature with jwt
    return jwt.sign(data, secret_key, {
        expiresIn: '1h'
    })
}

module.exports.verify = (req, res, next) => {
	// the token is retrieved from the request header
	let token = req.headers.authorization;

	// check if token is empty
	if(typeof token === "undefined"){
		return res.send({ auth: "Failed. No token" });
	} 

    console.log(token);
    //slice(<startingPosition>, <endPosition> )
    token = token.slice(7, token.length)

    //Validate the token using the "verify" method decrypting the token using the secret code
    jwt.verify(token, secret_key, function(err, decoded_token) {
        if(err){
            return res.send({
                auth: "Failed",
                message: err.message
            })
        }
        
        console.log(decoded_token); //contains the data from our token
        //user property will be added to request object and will contain our decodedToken. It can be accessed in the verify
        req.user = decoded_token

        //next() will let us proceed to the next middleware Or controller
        next();
    })
}

module.exports.verifyAdmin = (request, response, next) => {
    if(request.user.is_admin){
        next()
    } else {
        return response.send({
            auth: 'Failed',
            message: 'Action Forbidden'
        })
    }
}