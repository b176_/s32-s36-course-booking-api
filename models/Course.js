const mongoose = require('mongoose')

const course_schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'is Required']
    },
    description: {
        type: String,
        required: [true, 'is Required']
    },
    price: {
        type: Number,
        required: [true, 'Course Price is Required']
    },
    is_active: {
        type: Boolean,
        default: true
    }, 
    created_on: {
        type: Date,
        default: new Date()
    },  
    enrollees: [
        {
            user_id: {
                type: String,
                required: [true, 'Student ID is Required']
            },
            enrolled_on: {
                type: Date,
                default: new Date()
            }
        }
    ]
});

module.exports = mongoose.model('Course', course_schema);