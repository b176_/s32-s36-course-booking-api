const User = require('../models/User')
const bcrypt = require('bcrypt')
const dotenv = require('dotenv')
const auth = require('../auth')
const res = require('express/lib/response')

dotenv.config()

module.exports.register = (user_data) => {
    let new_user = new User({
        first_name: user_data.first_name,
        last_name: user_data.last_name,
        email: user_data.email,
        password: bcrypt.hashSync(user_data.password, parseInt(process.env.SALT)),
        mobile_number: user_data.mobile_number
    })

    return new_user.save().then((user, error) => {
        if (user) {
            return 'User registered successfully!'
        } else {
            return error
        }
    })
}

module.exports.login = (user_data) => {

    // check if user exists
    return User.findOne({
        email: user_data.email
    }).then(result => {
        if(!result){
            return false 
        }

        const password_match = bcrypt.compareSync(user_data.password, result.password)

        if(password_match){
            // convert result to an object
            return {accessToken: auth.createAccessToken(result.toObject())}
        } else {
            return false
        }
    })
}

module.exports.getUser = (user_id) => {
    return User.findById(user_id)
        .then(result => {
            // change value of password to empty string
            result.password = '';
            return result;
        })
        .catch(err => err.message);
}

module.exports.enroll = async (request, response) => {
    if(request.user.is_admin) {
        return response.send({ message: 'Action Forbidden' })
    }

    let updated_user = await User.findById(request.user.id).then(user => {
        user.enrollments.push({
            course_id: request.body.course_id
        })

        return user.save().then(saved_user => {
            return response.send(saved_user)
        }).catch(error => error.message)
        
        if(updated_user !== true) {
            return response.send({ message: updated_user})
        }
    })
}