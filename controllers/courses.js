const Course = require('../models/Course')
const router = require('../routes/users')

module.exports.addOne = (course_data) => {
    let new_course = new Course({
        name: course_data.name,
        description: course_data.description,
        price: course_data.price
    })

    return new_course.save().then((saved_course, error) => {
        if(error) {
            return false 
        }

        return new_course
    }).catch(error => {
        return error.message
    })
}

module.exports.getAll = () => {
    return Course.find({}).then(result => {
        return result
    }).catch(error => {
        return error.message
    })
}

module.exports.getAllActive = () => {
    return Course.find({
        is_active: true
    }).then(result => {
        return result
    }).catch(error => {
        return error.message
    })
}

module.exports.getOne = (course_id) => {
    return Course.findById(course_id).then(result => {
        return result 
    }).catch(error => {
        return error.message
    })
}

module.exports.updateOne = (course_id, new_data) => {
    let updated_course = {
        name: new_data.name,
        description: new_data.description,
        price: new_data.price
    }

    return Course.findByIdAndUpdate(course_id, updated_course).then((result, error) => {
        if(error) {
            return error
        }

        return result
    }).catch(error => {
        return error.message
    })
}

module.exports.archiveOne = (course_id) => {
    return Course.findByIdAndUpdate(course_id, {
        is_active: false
    }).then((result, error) => {
        if(error) {
            return error
        }

        return result
    }).catch(error => {
        return error.message
    })
}